package org.domco.connectfour.console.presentation;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Game Parameters to populate the Game State domain object on its initial creation.
 */
public class GameParameters {

    private final int gameWidth;

    private final int gameHeight;

    private final int consecutivePiecesForWin;

    private final int playerTwoHandicap;

    private GameParameters(GameParametersBuilder builder) {
        this.gameWidth = builder.gameWidth;
        this.gameHeight = builder.gameHeight;
        this.consecutivePiecesForWin = builder.consecutivePiecesForWin;
        this.playerTwoHandicap = builder.playerTwoHandicap;
    }

    int getGameWidth() {
        return gameWidth;
    }

    int getGameHeight() {
        return gameHeight;
    }

    int getConsecutivePiecesForWin() {
        return consecutivePiecesForWin;
    }

    int getPlayerTwoHandicap() {
        return playerTwoHandicap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GameParameters that = (GameParameters) o;

        return new EqualsBuilder()
                .append(gameWidth, that.gameWidth)
                .append(gameHeight, that.gameHeight)
                .append(consecutivePiecesForWin, that.consecutivePiecesForWin)
                .append(playerTwoHandicap, that.playerTwoHandicap)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(gameWidth)
                .append(gameHeight)
                .append(consecutivePiecesForWin)
                .append(playerTwoHandicap)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gameWidth", gameWidth)
                .append("gameHeight", gameHeight)
                .append("consecutivePiecesForWin", consecutivePiecesForWin)
                .append("playerTwoHandicap", playerTwoHandicap)
                .toString();
    }

    /**
     * Builder for the GameState class
     */
    static class GameParametersBuilder {

        private int gameWidth;

        private int gameHeight;

        private int consecutivePiecesForWin;

        private int playerTwoHandicap;

        GameParameters build() {
            return new GameParameters(this);
        }

        GameParameters.GameParametersBuilder withGameWidth(int gameWidth) {
            this.gameWidth = gameWidth;
            return this;
        }

        GameParameters.GameParametersBuilder withGameHeight(int gameHeight) {
            this.gameHeight = gameHeight;
            return this;
        }

        GameParameters.GameParametersBuilder withConsecutivePiecesForWin(int consecutivePiecesForWin) {
            this.consecutivePiecesForWin = consecutivePiecesForWin;
            return this;
        }

        GameParameters.GameParametersBuilder withPlayerTwoHandicap(int playerTwoHandicap) {
            this.playerTwoHandicap = playerTwoHandicap;
            return this;
        }

    }


}
