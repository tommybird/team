package org.domco.connectfour.server.controller.application.mbean;

import org.springframework.jmx.export.annotation.ManagedResource;

/**
 * Interface for Player Lookup Manager Bean
 */
@ManagedResource
public interface PlayerLookupManagerMBean {

    /**
     * Removes all players from the playerLookup map
     */
    void emptyPlayerLookupMap();
}
