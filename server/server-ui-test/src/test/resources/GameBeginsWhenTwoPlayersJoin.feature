Feature: When two users are put into a game the game will begin.

  Scenario: When the second player joins the game both player's screens will be updated and the game will begin.

    Given two users "Bart" and "Lisa" have chosen to join a game
    Then the game has the following game state information:
      | playerName | playerColour | opponentName | gameStatus   | gameHeight | gameWidth | colourJustPlayed |
      | Bart       | yellow       | Lisa         | undetermined | 6          | 7         |                  |
      | Lisa       | red          | Bart         | undetermined | 6          | 7         |                  |
    And the following pieces were played:
      | c0 | c1 | c2 | c3 | c4 | c5 | c6 |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |