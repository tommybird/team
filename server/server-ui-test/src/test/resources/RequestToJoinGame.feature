Feature: When a user navigates to the home page they get prompted to enter their name.

  Scenario: When a user tries to play the game they have to enter their name and wait for an opponent player to join.

    Given A player named "Nelson" navigates to the home page
    When The user selects to play a game
    Then They are informed they need to wait for another player to join and the game has the following information
      | playerName | playerColour | opponentName | gameStatus   | gameHeight | gameWidth | colourJustPlayed |
      | Nelson     | undefined    |              | undetermined | 0          | 0         |                  |