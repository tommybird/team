package org.domco.connectfour.server.ui.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Arrays;
import java.util.List;

/**
 * Game object that holds the state of the game throughout the test.
 */
public final class Game {

    private final List<GamePlayer> gamePlayerList;

    public Game(GameBuilder builder) {
        this.gamePlayerList = builder.gamePlayerlist;
    }

    public List<GamePlayer> getGamePlayerList() {
        return gamePlayerList;
    }

    /**
     * @return A new Game instance where only one player is involved in the test.
     */
    public static Game createNewSinglePlayerGameInstance(String playerName) {

        GamePlayer yellowPlayer = new GamePlayer.GamePlayerBuilder()
                .withPlayerName(playerName)
                .withPlayerColour(PlayerColour.YELLOW)
                .withGameState(new GameState.GameStateBuilder().build())
                .withGameStatus(GameStatus.UNDETERMINED)
                .build();

        List<GamePlayer> players = Arrays.asList(yellowPlayer);

        return new GameBuilder()
                .withGamePlayerList(players)
                .build();
    }

    /**
     * @return A new Game instance where two players are involved in the test.
     */
    public static Game createNewTwoPlayerGameInstance(String yellowPlayerName, String redPlayerName) {

        GamePlayer yellowPlayer = new GamePlayer.GamePlayerBuilder()
                .withPlayerName(yellowPlayerName)
                .withPlayerColour(PlayerColour.YELLOW)
                .withGameState(new GameState.GameStateBuilder().build())
                .withGameStatus(GameStatus.UNDETERMINED)
                .build();

        GamePlayer redPlayer = new GamePlayer.GamePlayerBuilder()
                .withPlayerName(redPlayerName)
                .withPlayerColour(PlayerColour.RED)
                .withGameState(new GameState.GameStateBuilder().build())
                .withGameStatus(GameStatus.UNDETERMINED)
                .build();

        List<GamePlayer> players = Arrays.asList(yellowPlayer, redPlayer);

        return new GameBuilder()
                .withGamePlayerList(players)
                .build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        return new EqualsBuilder()
                .append(gamePlayerList, game.gamePlayerList)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(gamePlayerList)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gamePlayerList", gamePlayerList)
                .toString();
    }

    /**
     * Builder for the Game class
     */
    public static class GameBuilder {

        private List<GamePlayer> gamePlayerlist;

        public Game build() {
            return new Game(this);
        }

        public GameBuilder withGamePlayerList(final List<GamePlayer> gamePlayerlist) {
            this.gamePlayerlist = gamePlayerlist;
            return this;
        }

    }
}
