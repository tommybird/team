package org.domco.connectfour.server.ui.presentation.service;

import com.frameworkium.core.ui.pages.PageFactory;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.domco.connectfour.server.ui.presentation.mappers.GamePagePresentationMapper;
import org.domco.connectfour.server.ui.presentation.domainobjects.GamePageObject;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

/**
 * Implementation for Game Play Presentation Service
 */
@Component
public class GamePlayPresentationServiceHandler implements GamePlayPresentationService {

    private static final Logger logger = LoggerFactory.getLogger(GameInitiationPresentationServiceHandler.class);

    private GamePagePresentationMapper gamePagePresentationMapper;

    private BrowserInteractionService browserInteractionService;

    @Inject
    public GamePlayPresentationServiceHandler(BrowserInteractionService browserInteractionService, GamePagePresentationMapper gamePagePresentationMapper) {
        this.browserInteractionService = browserInteractionService;
        this.gamePagePresentationMapper = gamePagePresentationMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<GamePlayer> dropPieceInColumn(PlayerColour playerColourToPlayMove,
                                              GamePlayer yellowPlayer, GamePlayer redPlayer, int columnNumber) {

        logger.debug("Attempting to drop piece for the " + playerColourToPlayMove + " player.");

        //Interacting with relevant player's game page
        if (playerColourToPlayMove.equals(PlayerColour.YELLOW)) {

            //Switch to the relevant player's tab and play the move
            browserInteractionService.switchTabFocus(PlayerColour.YELLOW);
            playPiece(columnNumber);

        } else if (playerColourToPlayMove.equals(PlayerColour.RED)) {

            //Switch to the relevant player's tab and play the move
            browserInteractionService.switchTabFocus(PlayerColour.RED);
            playPiece(columnNumber);

        } else {
            throw new IllegalStateException("The game player did not have a player colour");
        }

        //Update the player objects for both players
        browserInteractionService.switchTabFocus(PlayerColour.YELLOW);
        GamePlayer updatedYellowPlayer = gamePagePresentationMapper.mapToGamePlayer(yellowPlayer);

        browserInteractionService.switchTabFocus(PlayerColour.RED);
        GamePlayer updatedRedPlayer = gamePagePresentationMapper.mapToGamePlayer(redPlayer);

        List<GamePlayer> updatedGamePlayers = Arrays.asList(updatedYellowPlayer, updatedRedPlayer);

        logger.debug("Returning the updated Game Players from the Game Page Objects after a user has had their turn.");
        return updatedGamePlayers;

    }

    /**
     * Plays a piece in the player's chosen column using the page object to interact with the UI
     *
     * @param columnNumber         the column that the player wishes to play in
     */
    private void playPiece(int columnNumber) {

        PageFactory.newInstance(GamePageObject.class)
                .getGameGrid()
                .clickDropButtonHeaderCell(columnNumber, browserInteractionService.getDriver());

    }

}
