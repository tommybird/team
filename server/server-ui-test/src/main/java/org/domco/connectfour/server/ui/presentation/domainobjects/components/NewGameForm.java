package org.domco.connectfour.server.ui.presentation.domainobjects.components;

import com.frameworkium.core.ui.pages.PageFactory;
import org.domco.connectfour.server.ui.presentation.domainobjects.GamePageObject;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

/**
 * Component for all elements in the new game form
 */
@FindBy(id = "new_game_form")
public class NewGameForm extends HtmlElement {

    @FindBy(id = "player_name")
    private WebElement nameField;

    @FindBy(css = "input[value='Play a game']")
    private Button playAGame;

    /**
     * Once a player clicks to play then they are redirected to the game page.
     *
     * @param playerName name of the player registering to play
     * @return new instance of the game page.
     */
    public GamePageObject registerPlayerForGame(String playerName) {
        nameField.sendKeys(playerName);
        playAGame.click();
        return PageFactory.newInstance(GamePageObject.class);
    }

    private WebElement getNameField() {
        return nameField;
    }

    private Button getPlayAGame() {
        return playAGame;
    }
}
