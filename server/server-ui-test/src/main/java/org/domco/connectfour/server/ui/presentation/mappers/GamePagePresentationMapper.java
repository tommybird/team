package org.domco.connectfour.server.ui.presentation.mappers;

import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.presentation.domainobjects.GamePageObject;
import org.openqa.selenium.WebDriver;

/**
 * Interface for the Game Page Presentation Mapper
 */
public interface GamePagePresentationMapper {

    /**
     * @param gamePlayer           the game player who's page object is being mapped
     * @return the mapped player object from the Game Page.
     */
    GamePlayer mapToGamePlayer(GamePlayer gamePlayer);

}
