package org.domco.connectfour.server.ui.presentation.service;

import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.openqa.selenium.WebDriver;

import java.util.List;

/**
 * Interface for Game Play Presentation Service.
 */
public interface GamePlayPresentationService {

    /**
     * A piece will be played in the designated column for the designated player
     *
     * @param playerColourToPlayMove the player colour that will be performing the move
     * @param yellowGamePlayer       the yellow player
     * @param redGamePlayer          the red player
     * @param columnNumber           the column the player should drop the piece in
     * @return The list of updated game players after the move has been played.
     */
    List<GamePlayer> dropPieceInColumn(PlayerColour playerColourToPlayMove,
                                       GamePlayer yellowGamePlayer, GamePlayer redGamePlayer, int columnNumber);

}
