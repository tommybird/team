package org.domco.connectfour.server.ui.service;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Implementation of interface for the State of the game throughout each test.
 */
@Component
public class TestStateContainerHandler implements TestStateContainer {

    private static final Logger logger = LoggerFactory.getLogger(TestStateContainerHandler.class);

    //The Game object to contain the state of the game throughout the test.
    private Game game;

    /**
     * {@inheritDoc}
     */
    @Override
    public Game getGame() {
        return game;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addUpdatedPlayerToGameObject(GamePlayer gamePlayer, GamePlayer updatedGamePlayer) {
        logger.debug("Attempting to update Game object with the updated player");

        //Identify old player object that needs to be replaced in the game list of players.
        Optional<GamePlayer> gamePlayerOptional = getGame().getGamePlayerList()
                .stream()
                .filter(player -> player.getPlayerColour().equals(gamePlayer.getPlayerColour()))
                .findFirst();

        //Replace old player object with updated player object.
        if (gamePlayerOptional.isPresent()) {
            GamePlayer gamePlayerToReplace = gamePlayerOptional.get();
            getGame().getGamePlayerList()
                    .set(getGame().getGamePlayerList().indexOf(gamePlayerToReplace), updatedGamePlayer);
        } else {
            throw new IllegalStateException("Old player object to be replaced could not be found in the list of players.");
        }

        logger.debug("Updated Game object with the updated player");
    }

}
