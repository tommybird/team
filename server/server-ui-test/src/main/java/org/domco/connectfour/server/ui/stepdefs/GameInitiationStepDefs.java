package org.domco.connectfour.server.ui.stepdefs;

import com.frameworkium.core.ui.tests.BaseUITest;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.service.GameInitiationService;
import org.domco.connectfour.server.ui.service.TestStateContainer;
import org.domco.connectfour.server.ui.stepdefs.domainobjects.GameStateInformation;
import org.domco.connectfour.server.ui.stepdefs.mappers.GameInformationMapper;

import javax.inject.Inject;
import java.util.List;

/**
 * Step definitions for Game Initiation.
 */
public class GameInitiationStepDefs extends BaseUITest {

    private final TestStateContainer testStateContainer;
    private final GameInitiationService gameInitiationService;
    private final GameInformationMapper gameInformationMapper;


    @Inject
    public GameInitiationStepDefs(TestStateContainer testStateContainer, GameInitiationService gameInitiationService,
                                  GameInformationMapper gameInformationMapper) {
        this.testStateContainer = testStateContainer;
        this.gameInitiationService = gameInitiationService;
        this.gameInformationMapper = gameInformationMapper;
    }

    @When("^A player named \"([^\"]*)\" navigates to the home page$")
    public void aPlayerNamedNavigatesToTheHomePage(String playerName) {
        testStateContainer.setGame(Game.createNewSinglePlayerGameInstance(playerName));
        GamePlayer yellowPlayer = testStateContainer.getGame().getGamePlayerList().get(0);

        gameInitiationService.openHomePage(yellowPlayer);
    }

    @When("^The user selects to play a game$")
    public void theUserSelectsToPlayAGame() {
        GamePlayer yellowPlayer = testStateContainer.getGame().getGamePlayerList().get(0);
        gameInitiationService.requestToPlayAGame(yellowPlayer);
    }

    @Given("^two users \"([^\"]*)\" and \"([^\"]*)\" have chosen to join a game$")
    public void twoUsersAndHaveChosenToJoinAGame(String yellowPlayerName, String redPlayerName) {

        testStateContainer.setGame(Game.createNewTwoPlayerGameInstance(yellowPlayerName, redPlayerName));

        //Red player joins which starts the game and each player gets their game page updated.
        gameInitiationService.bothPlayersJoinToInitiateGame();

    }

    @Then("^They are informed they need to wait for another player to join and the game has the following information$")
    public void theyAreInformedTheyNeedToWaitForAnotherPlayerToJoinAndTheGameHasTheFollowingInformation(
            List<GameStateInformation> gameStateInformationList) {

        Game expectedGame = gameInformationMapper.mapToExpectedGame(gameStateInformationList);
        gameInitiationService.assertUserHasToWaitForOpponent(expectedGame);

    }

}
