package org.domco.connectfour.server.ui.service;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;

/**
 * Interface for Game Initiation Service
 */
public interface GameInitiationService {

    /**
     * Open the home page in the browser
     *
     * @param gamePlayer that is opening the home page
     * @return gamePlayer object that reflects the state of the player after the home page has been opened
     */
    GamePlayer openHomePage(GamePlayer gamePlayer);

    /**
     * Register player for a game on the home page
     *
     * @param gamePlayer that is registering for the game
     */
    void requestToPlayAGame(GamePlayer gamePlayer);

    /**
     * Opens the home page in a new browser instance
     * Registers the second playerName for a game on the homepage
     * This triggers the start of the game
     */
    void bothPlayersJoinToInitiateGame();

    /**
     * Compares the expected game object with the actual game object to confirm the game is in the correct state
     *
     * @param expectedGame the expected game object to be used in the assertion
     */
    void assertUserHasToWaitForOpponent(Game expectedGame);

}
