package org.domco.connectfour.server.model.mappers;

import org.domco.connectfour.game.domain.GamePiece;
import org.domco.connectfour.server.domain.Game;
import org.domco.connectfour.server.domain.GamePlayer;
import org.domco.connectfour.server.model.GamePieceModel;
import org.domco.connectfour.server.model.GameStateModel;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Game State Mapper Handler
 */
@Component
public class GameStateMapperHandler implements GameStateMapper {

    /**
     * {@inheritDoc}
     */
    @Override
    public GameStateModel mapToModel(final Game game, final String callingPlayerKey) {

        GameStateModel gameStateModel = null;

        if (null != game && null != game.getGameState()) {

            gameStateModel = new GameStateModel();

            final Set<GamePieceModel> gamePieceModelSet = new HashSet<>();
            for (final GamePiece gamePiece : game.getGameState().getGamePieceSet()) {
                final GamePieceModel gamePieceModel = new GamePieceModel();

                gamePieceModel.setxPosition(gamePiece.getxPosition());
                gamePieceModel.setyPosition(gamePiece.getyPosition());
                gamePieceModel.setPieceColour(gamePiece.getPieceColour()
                        .toString());

                gamePieceModelSet.add(gamePieceModel);
            }

            gameStateModel.setGamePieceSet(gamePieceModelSet);
            gameStateModel.setGameWidth(game.getGameState().getGameWidth());
            gameStateModel.setGameHeight(game.getGameState().getGameHeight());

            final GamePlayer gamePlayer = game.getGamePlayerList()
                    .stream()
                    .filter(p -> callingPlayerKey.equals(p.getKey()))
                    .findFirst()
                    .get();

            gameStateModel.setPlayerName(gamePlayer.getName());
            gameStateModel.setPlayerColour(gamePlayer.getPieceColour().toString());

            final GamePlayer opponentGamePlayer = game.getGamePlayerList()
                    .stream()
                    .filter(p -> !callingPlayerKey.equals(p.getKey()))
                    .findFirst()
                    .get();

            gameStateModel.setOpponentName(opponentGamePlayer.getName());
            gameStateModel.setOpponentColour(opponentGamePlayer.getPieceColour().toString());

            gameStateModel.setTurnColour(game.getGameState().getNextPieceColour().toString());

            gameStateModel.setGameStatus(game.getGameState().getGameStatus().toString());

            gameStateModel.setWinner(game.getWinner());
        }

        return gameStateModel;
    }
}
