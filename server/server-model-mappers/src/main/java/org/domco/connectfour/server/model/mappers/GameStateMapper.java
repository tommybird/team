package org.domco.connectfour.server.model.mappers;

import org.domco.connectfour.server.domain.Game;
import org.domco.connectfour.server.model.GameStateModel;

/**
 * Game State Mapper
 */
public interface GameStateMapper {

    /**
     * Map the Game object to GameStateModel object
     */
    GameStateModel mapToModel(final Game game, final String callingPlayerKey);
}
