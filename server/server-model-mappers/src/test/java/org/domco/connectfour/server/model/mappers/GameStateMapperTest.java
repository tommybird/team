package org.domco.connectfour.server.model.mappers;

import org.domco.connectfour.game.domain.GamePiece;
import org.domco.connectfour.game.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.game.domain.GameState;
import org.domco.connectfour.game.domain.GameState.GameStateBuilder;
import org.domco.connectfour.game.domain.PieceColour;
import org.domco.connectfour.server.domain.Game;
import org.domco.connectfour.server.domain.GamePlayer;
import org.domco.connectfour.server.model.GamePieceModel;
import org.domco.connectfour.server.model.GameStateModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GameStateMapperTest {

    /**
     * Class under test.
     */
    private GameStateMapper gameStateMapper;

    /**
     * Set of game pieces
     */
    private Set<GamePiece> gamePieceSet;

    /**
     *  Set of model game pieces
     */
    private Set<GamePieceModel> gamePieceModelSet;

    /**
     * Width of a standard game.
     */
    private static final int STANDARD_GAME_WIDTH = 7;

    /**
     * Height of a standard game.
     */
    private static final int STANDARD_GAME_HEIGHT = 6;

    /**
     * Number of consecutive pieces that are the same colour required for the game to be won.
     * For standard rules set equal to 4
     */
    private static final int CONSECUTIVE_PIECES_FOR_WIN = 4;

    /**
     * Player two handicap.
     * When HANDICAP = 0 both players get an equal amount of turns per go.
     */
    private static final int HANDICAP = 0;

    @Before
    public void setup() {

        gameStateMapper = new GameStateMapperHandler();
        gamePieceSet = new HashSet<>();
        gamePieceModelSet = new HashSet<>();

    }

    @Test
    public void checkMapToModelWithNull() {

        // Expectation
        final GameStateModel expectedGameStateModel = null;

        // Test
        final GameStateModel actualGameStateModel = gameStateMapper.mapToModel(null, null);

        // Assert
        Assert.assertEquals(expectedGameStateModel, actualGameStateModel);
    }

    @Ignore
    @Test
    public void checkMapToModelNoPieces() {

        // Setup
        final GameState gameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withGamePieceSet(gamePieceSet)
                .build();

        // Expectation
        final GameStateModel expectedGameStateModel = new GameStateModel();
        expectedGameStateModel.setGameWidth(STANDARD_GAME_WIDTH);
        expectedGameStateModel.setGameHeight(STANDARD_GAME_HEIGHT);
        expectedGameStateModel.setGamePieceSet(gamePieceModelSet);

        final Game game = new Game(gameState, null);

        // Test
        final GameStateModel actualGameStateModel = gameStateMapper.mapToModel(game, null);

        // Assert
        Assert.assertEquals(expectedGameStateModel, actualGameStateModel);
    }

    @Ignore
    @Test
    public void checkMapToModelWithPieces() {

        // Setup
        final GamePiece gamePiece1 = new GamePieceBuilder()
                .withxPosition(3)
                .withyPosition(3)
                .withPieceColour(PieceColour.RED)
                .build();

        final GamePiece gamePiece2 = new GamePieceBuilder()
                .withxPosition(3)
                .withyPosition(4)
                .withPieceColour(PieceColour.YELLOW)
                .build();

        gamePieceSet.add(gamePiece1);
        gamePieceSet.add(gamePiece2);

        final GameState gameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withGamePieceSet(gamePieceSet)
                .build();

        // Expectation
        final GamePieceModel gamePieceModel1 = new GamePieceModel();
        gamePieceModel1.setxPosition(3);
        gamePieceModel1.setyPosition(3);
        gamePieceModel1.setPieceColour("RED");

        final GamePieceModel gamePieceModel2 = new GamePieceModel();
        gamePieceModel2.setxPosition(3);
        gamePieceModel2.setyPosition(4);
        gamePieceModel2.setPieceColour("YELLOW");

        gamePieceModelSet.add(gamePieceModel1);
        gamePieceModelSet.add(gamePieceModel2);

        final GameStateModel expectedGameStateModel = new GameStateModel();
        expectedGameStateModel.setGameWidth(STANDARD_GAME_WIDTH);
        expectedGameStateModel.setGameHeight(STANDARD_GAME_HEIGHT);
        expectedGameStateModel.setGamePieceSet(gamePieceModelSet);

        final Game game = new Game(gameState, null);

        // Test
        final GameStateModel actualGameStateModel = gameStateMapper.mapToModel(game, null);

        // Assert
        Assert.assertEquals(expectedGameStateModel, actualGameStateModel);
    }

    @Test
    public void checkMapToModel(){

        //Setup
        final GamePiece gamePiece1 = new GamePieceBuilder()
                .withxPosition(0)
                .withyPosition(0)
                .withPieceColour(PieceColour.RED)
                .build();

        final GamePiece gamePiece2 = new GamePieceBuilder()
                .withxPosition(3)
                .withyPosition(0)
                .withPieceColour(PieceColour.YELLOW)
                .build();

        gamePieceSet.add(gamePiece1);
        gamePieceSet.add(gamePiece2);

        final GameState gameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        GamePlayer playerOne = new GamePlayer("playerOne");
        playerOne.setPieceColour(PieceColour.RED);
        GamePlayer opponentOne = new GamePlayer("opponentOne");
        opponentOne.setPieceColour(PieceColour.YELLOW);

        final List<GamePlayer> gamePlayerList = Arrays.asList(playerOne, opponentOne);

        final Game game = new Game(gameState, gamePlayerList);

        //Expectation
        final GamePieceModel gamePieceModel1 = new GamePieceModel();
        gamePieceModel1.setxPosition(0);
        gamePieceModel1.setyPosition(0);
        gamePieceModel1.setPieceColour("RED");

        final GamePieceModel gamePieceModel2 = new GamePieceModel();
        gamePieceModel2.setxPosition(3);
        gamePieceModel2.setyPosition(0);
        gamePieceModel2.setPieceColour("YELLOW");

        gamePieceModelSet.add(gamePieceModel1);
        gamePieceModelSet.add(gamePieceModel2);

        final GameStateModel expectedGameStateModel = new GameStateModel();
        expectedGameStateModel.setPlayerName(playerOne.getName());
        expectedGameStateModel.setOpponentName(opponentOne.getName());
        expectedGameStateModel.setPlayerColour(playerOne.getPieceColour().toString());
        expectedGameStateModel.setTurnColour(playerOne.getPieceColour().toString());
        expectedGameStateModel.setGameStatus("UNDETERMINED");
        expectedGameStateModel.setWinner(null);
        expectedGameStateModel.setGameWidth(STANDARD_GAME_WIDTH);
        expectedGameStateModel.setGameHeight(STANDARD_GAME_HEIGHT);
        expectedGameStateModel.setGamePieceSet(gamePieceModelSet);

        //Test
        final GameStateModel actualGameStateModel = gameStateMapper.mapToModel(game, playerOne.getKey());

        //Actual
        Assert.assertEquals(expectedGameStateModel, actualGameStateModel);

    }
}
