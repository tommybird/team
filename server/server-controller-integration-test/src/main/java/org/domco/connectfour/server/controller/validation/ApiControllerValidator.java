package org.domco.connectfour.server.controller.validation;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Player;

/**
 * Interface for the Api Controller Validator.
 */
public interface ApiControllerValidator {

    /**
     * Takes the player Id from the player parameter and checks it matches the UUID format.
     *
     * @param player player that has requested to join a game.
     */
    void assertPlayerIdIsPresent(Player player);

    /**
     * Checks that the actual state of the game is equal to our expected game
     *
     * @param expectedGame the expected state of the game for each player after the test.
     */
    void assertActualGameMatchesExpected(Game expectedGame);
}
