package org.domco.connectfour.server.controller.service;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Game.GameBuilder;
import org.domco.connectfour.server.controller.domain.Player;
import org.domco.connectfour.server.controller.domain.Player.PlayerBuilder;
import org.domco.connectfour.server.controller.domain.PlayerGameState;
import org.domco.connectfour.server.controller.integration.ApiControllerIntegration;
import org.domco.connectfour.server.controller.validation.ApiControllerValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Implementation for API Controller Service
 * Service to build data transfer objects for the integration layer to send to the controller API.
 */
@Component
public class ApiControllerServiceHandler implements ApiControllerService {

    private ApiControllerIntegration apiControllerIntegration;
    private ApiControllerValidator apiControllerValidator;
    private APITestStateContainer apiTestStateContainer;

    private static final Logger logger = LoggerFactory.getLogger(ApiControllerServiceHandler.class);


    @Inject
    public ApiControllerServiceHandler(ApiControllerIntegration apiControllerIntegration, ApiControllerValidator apiControllerValidator,
                                       APITestStateContainer apiTestStateContainer) {
        this.apiControllerIntegration = apiControllerIntegration;
        this.apiControllerValidator = apiControllerValidator;
        this.apiTestStateContainer = apiTestStateContainer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Player playerRequestsToJoinGame(String playerName) {

        logger.debug("Attempting to send request for player to join game");

        String playerId = apiControllerIntegration
                .playerRequestsToJoinGame(playerName);

        return new PlayerBuilder()
                .withPlayerName(playerName)
                .withPlayerId(playerId)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Game retrievePlayersGameStates(List<Player> players) {

        logger.debug("Retrieving player game states for each player");

        List<PlayerGameState> playerPlayerGameStates = new ArrayList<>();
        for (Player currentPLayer : players) {
            PlayerGameState playerPlayerGameState = apiControllerIntegration.retrievePlayersGameState(currentPLayer.getPlayerId());
            playerPlayerGameStates.add(playerPlayerGameState);
        }

        //Update actual state of game
        Game actualGame = new GameBuilder()
                .withPlayers(players)
                .withPlayerGameStateList(playerPlayerGameStates)
                .build();

        logger.debug("Returning updated actual game object");

        return actualGame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void playGamePiece(String playerName, int columnNum) {

        logger.debug("Attempting to play a game move for the given player");

        final Optional<Player> playerOptional = apiTestStateContainer.getGame().getPlayers().stream()
                .filter(player -> player.getPlayerName().equals(playerName))
                .findFirst();

        String playerId;
        if (playerOptional.isPresent()) {
            final Player player = playerOptional.get();
            playerId = player.getPlayerId();
        } else {
            throw new IllegalStateException("The given player name did not equal one of the actual player names.");
        }

        PlayerGameState updatedPlayerGameState = apiControllerIntegration.playGamePiece(playerId, columnNum);

        //Updating actual PlayerGameState objects with updated move
        Game actualGame = retrievePlayersGameStates(apiTestStateContainer.getGame().getPlayers());
        apiTestStateContainer.setGame(actualGame);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assertPlayerGameStates(Game expectedGame) {
        apiControllerValidator.assertActualGameMatchesExpected(expectedGame);
    }

    @Override
    public void assertPlayerIdIsPresent(Player currentPlayer) {
        apiControllerValidator.assertPlayerIdIsPresent(currentPlayer);
    }
}
