package org.domco.connectfour.server.controller.stepdefs.mappers;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.stepdefs.domainobjects.GameRowInformation;
import org.domco.connectfour.server.controller.stepdefs.domainobjects.GameStateInformation;

import java.util.List;

/**
 * Game State Information Mapper Interface.
 */
public interface GameStateInformationMapper {

    /**
     * No gameRowInformationList is passed in as the game has not begun.
     *
     * @param gameStateInformationList state of the game including player information, height, width and who just played a piece
     * @return expected game object for assertion at the end of a test
     */
    Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList);

    /**
     * No gameRowInformationList is passed in as the game has not begun.
     *
     * @param gameStateInformationList state of the game including player information, height, width and who just played a piece
     * @param gameRowInformationList   the pieces played on the game grid
     * @return expected game object for assertion at the end of a test
     */
    Game mapToExpectedGame(List<GameStateInformation> gameStateInformationList, List<GameRowInformation> gameRowInformationList);

}
