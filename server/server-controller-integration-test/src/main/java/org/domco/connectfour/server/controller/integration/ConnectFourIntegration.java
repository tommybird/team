package org.domco.connectfour.server.controller.integration;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Method;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import org.domco.connectfour.server.controller.constants.ConnectFourEndpoint;
import org.domco.connectfour.server.controller.domain.ConnectfourDTO;

/**
 * Integration class for Connect4 specific services
 */
public class ConnectFourIntegration {


    /**
     * @return a Rest Assured Request Specification with the baseUri
     */
    private RequestSpecification getRequestSpec() {
        return RestAssured.given()
                .baseUri(ConnectFourEndpoint.BASE_URI)
                .relaxedHTTPSValidation() // trusts even invalid certs
                .log().all() // uncomment to log each request
                .contentType(ContentType.JSON)
                .accept(ContentType.JSON);
    }

    /**
     * @return a Rest Assured Response Specification with basic checks
     */
    protected ResponseSpecification getResponseSpec() {
        return RestAssured.expect().response()
                .statusCode(HttpStatus.SC_OK);
    }

    protected ValidatableResponse get(String url) {
        return request(Method.GET, url);
    }

    protected ValidatableResponse post(String url, ConnectfourDTO<?> body){
        return request(Method.POST, url, body);
    }

    ValidatableResponse delete(String url) {return request(Method.DELETE, url);}

    private ValidatableResponse request(Method post, String url, ConnectfourDTO<?> body) {
        return getRequestSpec()
                .when()
                .body(body)
                .request(post, url)
                .then();
    }

    private ValidatableResponse request(Method get, String url){
        return getRequestSpec()
                .when()
                .request(get, url)
                .then();
    }

}
