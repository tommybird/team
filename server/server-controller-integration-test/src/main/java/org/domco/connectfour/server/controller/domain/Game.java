package org.domco.connectfour.server.controller.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

/**
 * Game object that holds the state of the game throughout the test.
 */
public final class Game {

    private final List<Player> players;

    private final List<PlayerGameState> playerGameStateList;

    public Game(GameBuilder builder) {
        this.players = builder.players;
        this.playerGameStateList = builder.playerGameStateList;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public List<PlayerGameState> getPlayerGameStateList() {
        return playerGameStateList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Game game = (Game) o;

        return new EqualsBuilder()
                .append(players, game.players)
                .append(playerGameStateList, game.playerGameStateList)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(players)
                .append(playerGameStateList)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("players", players)
                .append("playerGameStateList", playerGameStateList)
                .toString();
    }

    /**
     * Builder for the Game class
     */
    public static class GameBuilder {

        private List<Player> players;

        private List<PlayerGameState> playerGameStateList;

        public Game build() {
            return new Game(this);
        }

        public GameBuilder withPlayers (final List<Player> players) {
            this.players = players;
            return this;
        }

        public GameBuilder withPlayerGameStateList(final List<PlayerGameState> playerGameStateList) {
            this.playerGameStateList = playerGameStateList;
            return this;
        }

    }

}
