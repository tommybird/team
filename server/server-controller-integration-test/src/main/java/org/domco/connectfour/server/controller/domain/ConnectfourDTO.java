package org.domco.connectfour.server.controller.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Data transfer object for all data objects to extend from.
 */
public class ConnectfourDTO<T extends ConnectfourDTO> {

    /**
     * Log.
     */
    private static final Logger logger = LoggerFactory.getLogger(ConnectfourDTO.class);

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    /**
     * This method makes a clone of any object it is given.
     * Use to clone an object you receive as a response before
     * modifying instead of directly modifying the response.
     */
    public static Object clone(Object object) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
            ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
            ObjectInputStream ois = new ObjectInputStream(bais);
            return ois.readObject();
        } catch (Exception e) {
            logger.debug("The clone operation has failed on: {}", object);
            throw new IllegalStateException();
        }
    }

}
