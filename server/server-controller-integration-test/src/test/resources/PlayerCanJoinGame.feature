Feature: When a player requests to join a game they are given a player key

  Scenario: client makes a call to the api controller to join a game when no other players are in the game
    When "yellowPlayer" tries to join a new game
    Then "yellowPlayer" is given a player ID