package org.domco.connectfour.game.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.domco.connectfour.game.domain.GameMove.GameMoveBuilder;
import org.domco.connectfour.game.domain.GamePiece.GamePieceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Game State
 */
public final class GameState {

    private final int gameWidth;

    private final int gameHeight;

    //Colour of the most recently player piece.
    private final PieceColour colourPlayed;

    private final int consecutivePiecesForWin;

    private final int playerTwoHandicap;

    private final Set<GamePiece> gamePieceSet;

    private static final Logger logger = LoggerFactory.getLogger(GameState.class);

    private GameState(final GameStateBuilder builder) {
        this.gameWidth = builder.gameWidth;
        this.gameHeight = builder.gameHeight;
        this.colourPlayed = builder.colourPlayed;
        this.consecutivePiecesForWin = builder.consecutivePiecesForWin;
        this.playerTwoHandicap = builder.playerTwoHandicap;
        this.gamePieceSet = Collections.unmodifiableSet(builder.gamePieceSet);
    }

    //Game State logic.

    public Set<GameMove> calculateAvailableMoves() {

        logger.debug("Starting to calculating the set of available moves.");

        final Set<GameMove> availableGameMoves = new HashSet<>();
        final Set<GamePiece> gamePieceSet = getGamePieceSet();

        // Loop through all columns.
        for (int columnBeingChecked = 0; columnBeingChecked < getGameWidth(); columnBeingChecked++) {

            logger.debug("Creating a set of game pieces per column in the game state");

            final Set<GamePiece> gamePieceSetForColumn = new HashSet<>();
            for (final GamePiece currentGamePiece : gamePieceSet) {
                if (currentGamePiece.getxPosition() == columnBeingChecked) {
                    gamePieceSetForColumn.add(currentGamePiece);
                }
            }

            Integer yPosition = 0;

            if (!gamePieceSetForColumn.isEmpty()) {

                logger.debug("Calculating if this column has any space left in it");
                final Set<Integer> takenyPositionsInCurrentColumn = new HashSet<>();
                for (final GamePiece currentGamePiece : gamePieceSetForColumn) {
                    takenyPositionsInCurrentColumn.add(currentGamePiece.getyPosition());
                }

                yPosition = Collections.max(takenyPositionsInCurrentColumn) + 1;
            }

            // If the column has space in it then add a piece.
            if (yPosition < getGameHeight()) {

                logger.debug("Creating new game piece for this column and adding to existing game piece set");

                final GamePiece newGamePiece = new GamePieceBuilder().withxPosition(columnBeingChecked)
                        .withyPosition(yPosition)
                        .withPieceColour(pieceColourToPlayNextTurn())
                        .build();
                final Set<GamePiece> updatedGamePieceSet = new HashSet<>(gamePieceSet);
                updatedGamePieceSet.add(newGamePiece);

                final GameState endGameState = new GameStateBuilder().withGameWidth(getGameWidth())
                        .withGameHeight(getGameHeight())
                        .withColourPlayed(newGamePiece.getPieceColour())
                        .withConsecutivePiecesForWin(getConsecutivePiecesForWin())
                        .withPlayerTwoHandicap(getPlayerTwoHandicap())
                        .withGamePieceSet(updatedGamePieceSet)
                        .build();

                availableGameMoves.add(new GameMoveBuilder().withStartGameState(this)
                        .withEndGameState(endGameState)
                        .withPlayedPiece(newGamePiece)
                        .build());
            }
        }

        logger.debug("Returning the set of possible game moves.");
        return availableGameMoves;

    }

    public PieceColour pieceColourToPlayNextTurn() {

        final int playerTwoHandicap = getPlayerTwoHandicap() + 2;

        PieceColour colourToPlayNextTurn;
        if (getGamePieceSet().size() % playerTwoHandicap == 0) {
            colourToPlayNextTurn = PieceColour.RED;
        } else {
            colourToPlayNextTurn = PieceColour.YELLOW;
        }

        return colourToPlayNextTurn;
    }

    /**
     * Calculates if a player has won the game, the game is tied or still undetermined.
     *
     * @return GameStatus for the status of the game.
     */
    public GameStatus getGameStatus() {

        logger.debug("Checking game status.");

        GameStatus gameStatus = GameStatus.UNDETERMINED;

        //check for a tie.
        final GameStatus tiedGameStatus = checkForTiedGame();

        //Check for horizontal win in each row.
        final GameStatus horizontalGameStatus = checkHorizontal(this);

        //Check for vertical win in each column.
        final GameStatus verticalGameStatus = checkVertical(this);

        //Check for negative gradient diagonal win.
        final GameStatus negativeGradientGameStatus = checkForNegativeGradientDiagonalWin(this);

        //Check for positive gradient diagonal win.
        final GameStatus positiveGradientGameStatus = checkForPositiveGradientDiagonalWin(this);

        logger.debug("Returning game status.");

        if (tiedGameStatus != GameStatus.UNDETERMINED) {
            gameStatus = tiedGameStatus;
        }

        if (horizontalGameStatus != GameStatus.UNDETERMINED) {
            gameStatus = horizontalGameStatus;
        }

        if (verticalGameStatus != GameStatus.UNDETERMINED) {
            gameStatus = verticalGameStatus;
        }

        if (negativeGradientGameStatus != GameStatus.UNDETERMINED) {
            gameStatus = negativeGradientGameStatus;
        }

        if (positiveGradientGameStatus != GameStatus.UNDETERMINED) {
            gameStatus = positiveGradientGameStatus;
        }

        return gameStatus;
    }

    /**
     * Checks for a tied game status
     *
     * @return GameStatus whether the game is over due to a tie.
     */
    private GameStatus checkForTiedGame() {

        logger.debug("Checking if the game is a tie.");

        GameStatus gameStatus = GameStatus.UNDETERMINED;

        final int maxPlayablePositions = gameHeight*gameWidth;

        if(maxPlayablePositions == gamePieceSet.size()){
            gameStatus = GameStatus.TIE;
        }

        return gameStatus;

    }

    /**
     * Checks for a horizontal win
     *
     * @param gameState current state of the game
     * @return GameStatus whether the game is over due to a horizontal win.
     */
    private GameStatus checkHorizontal(final GameState gameState) {

        logger.debug("Checking if the game has been won horizontally.");

        GameStatus gameStatus = GameStatus.UNDETERMINED;

        for (int i = 0; i < gameState.getGameHeight(); i++) {

            final int row = i;

            //Sort by x position then check for four consecutive matching colours.
            final List<GamePiece> gamePiecesInRow = gameState.getGamePieceSet()
                    .stream()
                    .filter(gamePiece -> gamePiece.getyPosition() == row)
                    .sorted(Comparator.comparingInt(GamePiece::getxPosition))
                    .collect(Collectors.toList());

            gameStatus = checkForHorizontalWin(gamePiecesInRow, gameState);

            if (gameStatus != GameStatus.UNDETERMINED) {
                break;
            }

        }

        return gameStatus;

    }

    /**
     * Checks for a vertical win
     *
     * @param gameState current state of the game
     * @return GameStatus whether the game is over due to a vertical win.
     */
    private GameStatus checkVertical(final GameState gameState) {

        logger.debug("Checking if the game has been won vertically.");

        GameStatus gameStatus = GameStatus.UNDETERMINED;

        for (int i = 0; i < gameState.getGameWidth(); i++) {

            final int column = i;

            //Sort by y position then check for four consecutive matching colours.
            final List<GamePiece> gamePiecesInColumn = gameState.getGamePieceSet()
                    .stream()
                    .filter(gamePiece -> gamePiece.getxPosition() == column)
                    .sorted(Comparator.comparingInt(GamePiece::getyPosition))
                    .collect(Collectors.toList());

            gameStatus = checkForVerticalWin(gamePiecesInColumn, gameState);

            if (gameStatus != GameStatus.UNDETERMINED) {
                break;
            }

        }

        return gameStatus;

    }

    /**
     * Checks for a positive gradient diagonal win
     *
     * @param gameState current state of the game
     * @return GameStatus whether the game is over due to a positive gradient diagonal win.
     */
    private GameStatus checkForPositiveGradientDiagonalWin(final GameState gameState) {

        logger.debug("Checking if the game has been won on a positive diagonal.");

        GameStatus gameStatus = GameStatus.UNDETERMINED;

        //Iterate through each column, for each piece check if the piece in column to right is 1 up and has same colour
        for (int i = 0; i < gameState.getGameWidth(); i++) {

            final int column = i;
            final List<GamePiece> winningDiagonalPieceList = new ArrayList<>();

            final List<GamePiece> piecesInColumn = gameState.getGamePieceSet()
                    .stream()
                    .filter(gamePiece -> gamePiece.getxPosition() == column)
                    .sorted(Comparator.comparingInt(GamePiece::getyPosition))
                    .collect(Collectors.toList());

            for (final GamePiece gamePiece : piecesInColumn) {
                winningDiagonalPieceList.clear();
                gameStatus = calculateDiagonalPieceList(gameState, gamePiece, winningDiagonalPieceList, 1, gameStatus);

                if (gameStatus != GameStatus.UNDETERMINED) {
                    break;
                }

            }
        }

        return gameStatus;

    }

    /**
     * Checks for a negative gradient diagonal win
     *
     * @param gameState current state of the game
     * @return GameStatus whether the game is over due to a negative gradient diagonal win.
     */
    private GameStatus checkForNegativeGradientDiagonalWin(final GameState gameState) {

        logger.debug("Checking if the game has been won on a negative diagonal.");

        GameStatus gameStatus = GameStatus.UNDETERMINED;

        //Iterate through each column, for each piece check if the piece in column to right is 1 up and has same colour
        for (int i = gameState.getGameWidth() - 1; i >= 0; i--) {

            final int column = i;
            final List<GamePiece> winningDiagonalPieceList = new ArrayList<>();

            final List<GamePiece> piecesInColumn = gameState.getGamePieceSet()
                    .stream()
                    .filter(gamePiece -> gamePiece.getxPosition() == column)
                    .sorted(Comparator.comparingInt(GamePiece::getyPosition))
                    .collect(Collectors.toList());

            for (final GamePiece gamePiece : piecesInColumn) {
                winningDiagonalPieceList.clear();
                gameStatus = calculateDiagonalPieceList(gameState, gamePiece, winningDiagonalPieceList, -1, gameStatus);

                if (gameStatus != GameStatus.UNDETERMINED) {
                    break;
                }

            }
        }

        return gameStatus;

    }

    /**
     * Calculates if the supplied list has the same PieceColour for four pieces in a row
     *
     * @param gamePiecesInRow List of game pieces for this specific row
     * @return GameStatus whether the game is over due to a horizontal win.
     */
    private GameStatus checkForHorizontalWin(final List<GamePiece> gamePiecesInRow, final GameState gameState) {
        PieceColour pieceColour = null;
        int matchingColourPeices = 0;
        final List<GamePiece> winningPieces = new ArrayList<>();
        boolean horizontalWin = false;
        GameStatus gameStatus;

        for (final GamePiece currentPiece : gamePiecesInRow) {

            //Check if four pieces have the same colour in a row
            if (pieceColour == null) {
                pieceColour = currentPiece.getPieceColour();
                matchingColourPeices += 1;
                winningPieces.add(currentPiece);
            } else {
                if (pieceColour != currentPiece.getPieceColour()) {
                    winningPieces.clear();
                    pieceColour = currentPiece.getPieceColour();
                    matchingColourPeices = 1;
                    winningPieces.add(currentPiece);
                } else {
                    matchingColourPeices += 1;
                    winningPieces.add(currentPiece);
                }
            }

            if (matchingColourPeices == gameState.getConsecutivePiecesForWin()) {
                //Check the winning pieces are next to each other before awarding the win.
                horizontalWin = winningPiecesAreConsecutive(winningPieces);
                break;
            }
        }

        if (horizontalWin) {
            if (winningPieces.get(0).getPieceColour().equals(PieceColour.RED)) {
                gameStatus = GameStatus.RED_WIN;
            } else if (winningPieces.get(0).getPieceColour().equals(PieceColour.YELLOW)) {
                gameStatus = GameStatus.YELLOW_WIN;
            } else {
                throw new IllegalStateException("A horizontal win has been recorded but it cannot be deduced which player has won.");
            }
        } else {
            gameStatus = GameStatus.UNDETERMINED;
        }

        return gameStatus;

    }

    /**
     * Calculates if the supplied list has the same PieceColour for four pieces in a row
     *
     * @param gamePiecesInColumn List of game pieces for this specific column
     * @return GameStatus whether the game is over due to a vertical win.
     */
    private GameStatus checkForVerticalWin(final List<GamePiece> gamePiecesInColumn, final GameState gameState) {
        PieceColour pieceColour = null;
        int consecutiveMatchingColour = 0;
        boolean verticalWin = false;
        GameStatus gameStatus;

        for (final GamePiece currentPiece : gamePiecesInColumn) {

            if (pieceColour == null) {
                pieceColour = currentPiece.getPieceColour();
                consecutiveMatchingColour += 1;
            } else {
                if (pieceColour != currentPiece.getPieceColour()) {
                    pieceColour = currentPiece.getPieceColour();
                    consecutiveMatchingColour = 1;
                } else {
                    consecutiveMatchingColour += 1;
                }
            }

            if (consecutiveMatchingColour == gameState.getConsecutivePiecesForWin()) {
                verticalWin = true;
                break;
            }
        }

        if (verticalWin) {
            if (pieceColour.equals(PieceColour.RED)) {
                gameStatus = GameStatus.RED_WIN;
            } else if (pieceColour.equals(PieceColour.YELLOW)) {
                gameStatus = GameStatus.YELLOW_WIN;
            } else {
                throw new IllegalStateException("A vertical win has been recorded but it cannot be deduced which player has won.");
            }
        } else {
            gameStatus = GameStatus.UNDETERMINED;
        }

        return gameStatus;

    }

    /**
     * @param gameState                current state of the game that we are checking for a diagonal win
     * @param gamePiece                the current game piece in the column that is being checked
     * @param winningDiagonalPieceList list that keeps track of how many consecutive diagonal pieces have been played
     * @param xIncrement               whether to check diagonally up or down
     * @param gameStatus               if the game is still in play, a tie or won
     * @return gameStatus              whether the game is over due to a diagonal win
     */
    private GameStatus calculateDiagonalPieceList(final GameState gameState, final GamePiece gamePiece,
            final List<GamePiece> winningDiagonalPieceList, final int xIncrement, GameStatus gameStatus) {

        final List<GamePiece> diagonalPieceList = gameState.getGamePieceSet()
                .stream()
                .filter(gp -> ((gp.getxPosition() == gamePiece.getxPosition() + xIncrement)
                        && (gp.getyPosition() == gamePiece.getyPosition() + 1)
                        && (gp.getPieceColour() == gamePiece.getPieceColour())))
                .collect(Collectors.toList());

        if (!diagonalPieceList.isEmpty() && diagonalPieceList.size() != 1) {
            throw new IllegalStateException("There cannot be two pieces in one position");
        }

        if (!diagonalPieceList.isEmpty()) {

            winningDiagonalPieceList.add(diagonalPieceList.get(0));

            if (diagonalPieceList.get(0).getxPosition() + xIncrement < gameState.getGameWidth()
                    && diagonalPieceList.get(0).getyPosition() + 1 < gameState.getGameHeight()) {
                calculateDiagonalPieceList(gameState, diagonalPieceList.get(0),
                        winningDiagonalPieceList, xIncrement, gameStatus);
            }

        }

        if (winningDiagonalPieceList.size() == gameState.getConsecutivePiecesForWin() - 1) {

            final boolean sameColour = winningDiagonalPieceList.stream()
                    .allMatch(piece -> piece.getPieceColour()
                            .equals(winningDiagonalPieceList.get(0).getPieceColour()));

            if (sameColour) {
                if (winningDiagonalPieceList.get(0).getPieceColour().equals(PieceColour.RED)) {
                    gameStatus = GameStatus.RED_WIN;
                } else if (winningDiagonalPieceList.get(0).getPieceColour().equals(PieceColour.YELLOW)) {
                    gameStatus = GameStatus.YELLOW_WIN;
                } else {
                    throw new IllegalStateException("A diagonal win has been recorded but it cannot be deduced which player has won.");
                }
            } else {
                gameStatus = GameStatus.UNDETERMINED;
            }

        }

        return gameStatus;

    }

    /**
     * Checks the winning pieces have no empty positions between them.
     *
     * @param winningPieces The list of GamePieces that are in a row with the same colour
     * @return gameOver with true if there are no empty spaces between pieces
     */
    private boolean winningPiecesAreConsecutive(final List<GamePiece> winningPieces) {

        boolean gameOver = true;
        Integer num = null;

        for (final GamePiece current : winningPieces) {

            if (num == null) {
                num = current.getxPosition();
            } else {
                if (!((current.getxPosition() - 1) == num)) {
                    gameOver = false;
                }
                num = current.getxPosition();
            }

        }

        return gameOver;

    }

    public int getGameWidth() {
        return gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public int getConsecutivePiecesForWin() {
        return consecutivePiecesForWin;
    }

    public int getPlayerTwoHandicap() {
        return playerTwoHandicap;
    }

    public Set<GamePiece> getGamePieceSet() {
        return gamePieceSet;
    }

    /**
     * Gets the colour that should play next.
     */
    public PieceColour getNextPieceColour() {

        final int playerTwoHandicap = getPlayerTwoHandicap() + 2;

        PieceColour colourToPlayNextTurn;
        if (getGamePieceSet().size() % playerTwoHandicap == 0) {
            colourToPlayNextTurn = PieceColour.RED;
        } else {
            colourToPlayNextTurn = PieceColour.YELLOW;
        }

        return colourToPlayNextTurn;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final GameState gameState = (GameState) o;

        return new EqualsBuilder()
                .append(gameWidth, gameState.gameWidth)
                .append(gameHeight, gameState.gameHeight)
                .append(colourPlayed, gameState.colourPlayed)
                .append(consecutivePiecesForWin, gameState.consecutivePiecesForWin)
                .append(playerTwoHandicap, gameState.playerTwoHandicap)
                .append(gamePieceSet, gameState.gamePieceSet)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(gameWidth)
                .append(gameHeight)
                .append(colourPlayed)
                .append(consecutivePiecesForWin)
                .append(playerTwoHandicap)
                .append(gamePieceSet)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gameWidth", gameWidth)
                .append("gameHeight", gameHeight)
                .append("colourPlayed", colourPlayed)
                .append("consecutivePiecesForWin", consecutivePiecesForWin)
                .append("playerTwoHandicap", playerTwoHandicap)
                .append("gamePieceSet", gamePieceSet)
                .toString();
    }

    /**
     * Builder for the GameState class
     */
    public static class GameStateBuilder {

        private int gameWidth;

        private int gameHeight;

        private PieceColour colourPlayed;

        private int consecutivePiecesForWin;

        private int playerTwoHandicap;

        private Set<GamePiece> gamePieceSet;

        public GameState build() {
            return new GameState(this);
        }

        public GameStateBuilder withGameWidth(final int gameWidth) {
            this.gameWidth = gameWidth;
            return this;
        }

        public GameStateBuilder withGameHeight(final int gameHeight) {
            this.gameHeight = gameHeight;
            return this;
        }

        public GameStateBuilder withColourPlayed(final PieceColour colourPlayed) {
            this.colourPlayed = colourPlayed;
            return this;
        }

        public GameStateBuilder withConsecutivePiecesForWin(final int consecutivePiecesForWin) {
            this.consecutivePiecesForWin = consecutivePiecesForWin;
            return this;
        }

        public GameStateBuilder withPlayerTwoHandicap(final int playerTwoHandicap) {
            this.playerTwoHandicap = playerTwoHandicap;
            return this;
        }

        public GameStateBuilder withGamePieceSet(final Set<GamePiece> gamePieceSet) {
            this.gamePieceSet = gamePieceSet;
            return this;
        }

    }
}
