package org.domco.connectfour.game.domain;

import org.domco.connectfour.game.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.game.domain.GameState.GameStateBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Test class for the GameState
 */
public class GameStateTest {

    /**
     * Set of game pieces
     */
    private Set<GamePiece> gamePieceSet;

    /**
     * Width of a standard game.
     */
    private static final int STANDARD_GAME_WIDTH = 7;

    /**
     * Height of a standard game.
     */
    private static final int STANDARD_GAME_HEIGHT = 6;

    /**
     * Number of consecutive pieces that are the same colour required for the game to be won.
     * For standard rules set equal to 4
     */
    private static final int CONSECUTIVE_PIECES_FOR_WIN = 4;

    /**
     * Player two handicap.
     * When HANDICAP = 0 both players get an equal amount of turns per go.
     */
    private static final int HANDICAP = 0;

    @Before
    public void setup() {
        gamePieceSet = new HashSet<>();
    }

    /**
     * Checks if game states are equal even if their (identical) pieces are in a
     * different order.
     */
    @Test
    public void checkAgnosticGamePieceOrdering() {

        // Setup
        final GamePiece gamePiece1 = new GamePieceBuilder()
                .withxPosition(1)
                .withyPosition(1)
                .withPieceColour(PieceColour.RED)
                .build();

        final GamePiece gamePiece2 = new GamePieceBuilder()
                .withxPosition(2)
                .withyPosition(2)
                .withPieceColour(PieceColour.YELLOW)
                .build();

        final Set<GamePiece> gamePieceSet1 = new HashSet<>();
        gamePieceSet1.add(gamePiece1);
        gamePieceSet1.add(gamePiece2);

        final Set<GamePiece> gamePieceSet2 = new HashSet<>();
        gamePieceSet2.add(gamePiece2);
        gamePieceSet2.add(gamePiece1);

        final GameState gameState1 = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withGamePieceSet(gamePieceSet1)
                .build();

        final GameState gameState2 = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withGamePieceSet(gamePieceSet2)
                .build();

        // Assert
        Assert.assertEquals(gameState1, gameState2);
    }

    /**
     * Checks to ensure the correct amount of moves are calculated.
     * Game state under test:
     * 5 |  |  |  |  |  |  |  |
     * 4 |  |  |  |  |  |  |  |
     * 3 |  |  |  |  |  |  |  |
     * 2 |  |  |  |  |  |  |  |
     * 1 |  |  |  |  |  |  |  |
     * 0 |  |  |R |  |  |  |  |
     * ...0  1  2  3  4  5  6
     */
    @Test
    public void checkCalculateAllAvailableMovesReturnsCorrectAmount() {

        //setup - create a game state with one piece in
        final PieceColour pieceColour = PieceColour.RED;
        final int xPosition = 2;
        final int yPosition = 0;
        final GamePiece gamePiece = new GamePieceBuilder()
                .withxPosition(xPosition)
                .withyPosition(yPosition)
                .withPieceColour(pieceColour)
                .build();

        gamePieceSet.add(gamePiece);
        final GameState gameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(gamePiece.getPieceColour())
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        //Test
        final Set<GameMove> actualGameMoves = gameState.calculateAvailableMoves();

        //Assert
        Assert.assertEquals(7, actualGameMoves.size());

    }

    /**
     * Checks that all possible moves are calculated correctly
     * Game state under test:
     * 5 |Y |  |  |  |  |  |  |
     * 4 |Y |  |  |  |  |  |  |
     * 3 |Y |  |  |  |  |  |  |
     * 2 |R |  |  |  |  |  |  |
     * 1 |R |  |  |  |  |  |  |
     * 0 |R |Y |Y |R |R |  |  |
     * ...0  1  2  3  4  5  6
     */
    @Test
    public void checkCalculateAllAvailableMovesReturnsCorrectAvailableMoves() {

        // Setup
        // 1)Create a game state with multiple pieces in so a column is complete.

        final List<GamePiece> preExistingPieces = Arrays.asList(

                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(5).withPieceColour(PieceColour.YELLOW).build()

        );

        // 2)Create the game state.
        gamePieceSet.addAll(preExistingPieces);
        final GameState startingGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        // 3) Create an expected set of possible positions to play and collect the set of end game states from this
        final Set<GamePiece> expectedPositionsToPlay = Stream.of(

                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.RED).build()

        ).collect(Collectors.toSet());

        final Set<GameState> allExpectedEndGameStates = new HashSet<>();

        for (final GamePiece currentGamePiece : expectedPositionsToPlay) {
            allExpectedEndGameStates.add(addGameMove(currentGamePiece));
        }

        //Test
        final Set<GameMove> actualGameMoves = startingGameState.calculateAvailableMoves();

        final Set<GameState> allActualEndGameStates = new HashSet<>();

        for (final GameMove currentGameMove : actualGameMoves) {
            allActualEndGameStates.add(currentGameMove.getEndGameState());
        }

        //Assert - Check end game states are the same for the two lists
        Assert.assertEquals(allExpectedEndGameStates, allActualEndGameStates);

    }

    /**
     * @param gamePiece game piece that you want to be used in a game move
     * @return new game state object after using gamePiece
     */
    private GameState addGameMove(final GamePiece gamePiece) {
        final Set<GamePiece> endGamePieceSet = new HashSet<>(gamePieceSet);
        endGamePieceSet.add(gamePiece);
        return new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(gamePiece.getPieceColour())
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(endGamePieceSet)
                .build();
    }


    /**
     * Checks that the handicap system of yellow player getting an extra move every go works properly.
     * Game state under test:
     * 5 |  |  |  |  |  |  |  |
     * 4 |  |  |  |  |  |  |  |
     * 3 |  |  |  |  |  |  |  |
     * 2 |  |  |  |  |  |  |  |
     * 1 |  |  |  |  |  |  |  |
     * 0 |R |Y |Y?|R?|  |  |  |
     * ...0  1  2  3  4  5  6
     * Then confirm red goes after two yellow moves.
     */
    @Test
    public void checkForHandicapOfOne() {

        //Setup
        //1)Set handicap to one
        final int handicap = 1;

        //2)Create starting game state.
        final List<GamePiece> initialPieces = Arrays.asList(
                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.YELLOW).build()
        );

        gamePieceSet.addAll(initialPieces);

        final GameState startingGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(handicap)
                .withGamePieceSet(gamePieceSet)
                .build();

        //2)Expected colour to play next
        PieceColour expectedColourToPlayNext = PieceColour.YELLOW;

        //Test - Even though yellow just played it should be yellows turn again.
        PieceColour actualColourToPlayNext = startingGameState.pieceColourToPlayNextTurn();

        //Assert
        Assert.assertEquals(expectedColourToPlayNext, actualColourToPlayNext);

        //3)Play the move and confirm it is Red's go afterwards.
        final GamePiece endGamePiece = new GamePiece.GamePieceBuilder()
                .withxPosition(3)
                .withyPosition(0)
                .withPieceColour(PieceColour.RED)
                .build();
        gamePieceSet.add(endGamePiece);

        final GameState endGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(handicap)
                .withGamePieceSet(gamePieceSet)
                .build();

        expectedColourToPlayNext = PieceColour.RED;

        //Test
        actualColourToPlayNext = endGameState.pieceColourToPlayNextTurn();

        //Assert
        Assert.assertEquals(expectedColourToPlayNext, actualColourToPlayNext);


    }

    /**
     * Checks that the handicap system of yellow player getting an extra move every go works properly.
     * Game state under test:
     * 5 |  |  |  |  |  |  |  |
     * 4 |R?|  |  |  |  |  |  |
     * 3 |Y?|  |  |  |  |  |  |
     * 2 |Y?|  |  |  |  |  |  |
     * 1 |Y |  |  |  |  |  |  |
     * 0 |R |  |  |  |  |  |  |
     * ...0  1  2  3  4  5  6
     * Then confirm red goes after two yellow moves.
     */
    @Test
    public void checkForHandicapOfTwo() {

        //Setup
        //1)Set handicap to one
        final int handicap = 2;

        //2)Create starting game state.
        final List<GamePiece> initialPieces = Arrays.asList(
                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(1).withPieceColour(PieceColour.YELLOW).build()
        );

        gamePieceSet.addAll(initialPieces);

        final GameState startingGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(handicap)
                .withGamePieceSet(gamePieceSet)
                .build();

        //2)Expected colour to play next
        PieceColour expectedColourToPlayNext = PieceColour.YELLOW;

        //Test - Even though yellow just played it should be yellows turn again.
        PieceColour actualColourToPlayNext = startingGameState.pieceColourToPlayNextTurn();

        //Assert
        Assert.assertEquals(expectedColourToPlayNext, actualColourToPlayNext);

        //3)Play another piece
        final GamePiece nextPiece = new GamePieceBuilder()
                .withxPosition(0)
                .withyPosition(2)
                .withPieceColour(PieceColour.YELLOW).build();
        gamePieceSet.add(nextPiece);

        final GameState middleGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(handicap)
                .withGamePieceSet(gamePieceSet)
                .build();

        //Test - Even though yellow has played twice in a row it should still be yellow's go next turn
        actualColourToPlayNext = middleGameState.pieceColourToPlayNextTurn();

        //Assert
        Assert.assertEquals(expectedColourToPlayNext, actualColourToPlayNext);

        //Play the move and confirm it is Red's go afterwards.
        final GamePiece endGamePiece = new GamePiece.GamePieceBuilder()
                .withxPosition(0)
                .withyPosition(4)
                .withPieceColour(PieceColour.RED)
                .build();
        gamePieceSet.add(endGamePiece);

        final GameState endGameState = new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(handicap)
                .withGamePieceSet(gamePieceSet)
                .build();

        expectedColourToPlayNext = PieceColour.RED;

        //Test
        actualColourToPlayNext = endGameState.pieceColourToPlayNextTurn();

        //Assert
        Assert.assertEquals(expectedColourToPlayNext, actualColourToPlayNext);

    }
}
