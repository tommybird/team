package org.domco.connectfour.game.domain;

import org.domco.connectfour.game.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.game.domain.GameState.GameStateBuilder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Game over service large grid tests.
 */
public class LargeGridGameStateTests {

    /**
     * Set of game pieces
     */
    private Set<GamePiece> gamePieceSet;

    /**
     * Player two handicap.
     * When HANDICAP = 0 both players get an equal amount of turns per go.
     */
    private static final int HANDICAP = 0;

    @Before
    public void setup() {
        gamePieceSet = new HashSet<>();
    }

    /**
     * Checks to ensure the game logic behaves correctly when the gird and win sizes are scaled up.
     * Positive diagonal win
     * Game state under test:
     * 14|  |  |  |  |  |  |  |  |  |  |  |  |  |  |R?|
     * 13|  |  |  |  |  |  |  |  |  |  |  |  |  |R |R |
     * 12|  |  |  |  |  |  |  |  |  |  |  |  |R |Y |Y |
     * 11|  |  |  |  |  |  |  |  |  |  |  |R |Y |R |R |
     * 10|  |  |  |  |  |  |  |  |  |  |R |R |R |Y |Y |
     * 9 |  |  |  |  |  |  |  |  |  |R |R |Y |Y |R |R |
     * 8 |  |  |  |  |  |  |  |  |R |Y |Y |R |R |Y |Y |
     * 7 |  |  |  |  |  |  |  |R |Y |R |R |Y |Y |R |R |
     * 6 |  |  |  |  |  |  |R |R |R |Y |Y |R |R |Y |Y |
     * 5 |  |  |  |  |  |R |R |Y |Y |R |R |Y |Y |R |R |
     * 4 |  |  |  |  |R |Y |Y |R |R |Y |Y |R |R |Y |Y |
     * 3 |  |  |  |R |Y |R |R |Y |Y |R |R |Y |Y |R |R |
     * 2 |  |  |R |R |R |Y |Y |R |R |Y |Y |R |R |Y |Y |
     * 1 |Y |R |R |Y |Y |R |R |Y |Y |R |R |Y |Y |R |R |
     * 0 |R |Y |Y |Y |Y |Y |Y |Y |Y |Y |Y |Y |Y |Y |Y |
     * ...0  1  2  3  4  5  6  7  8  9  10 11 12 13 14
     */
    @Test
    public void check15x15Connect15() {

        //Setup
        //1) Define the large grid and win condition
        final int gameWidth = 15;
        final int gameHeight = 15;
        final int consecutivePiecesRequiredForWin = 15;

        //2) Create a game state one move away from a win
        List<GamePiece> preExistingPieces = createInitialPieceListFor15x15Connect15();
        gamePieceSet.addAll(preExistingPieces);

        GameState startingGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        //Test
        //1)Check GameStatus is undetermined.
        GameStatus gameStatus = startingGameState.getGameStatus();
        Assert.assertEquals("There shouldn't be a positive gradient diagonal win yet", GameStatus.UNDETERMINED, gameStatus);

        //2)Play the move which will end the game.
        GamePiece finalPiece = new GamePieceBuilder()
                .withxPosition(14)
                .withyPosition(14)
                .withPieceColour(PieceColour.RED)
                .build();
        gamePieceSet.add(finalPiece);

        GameState endGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        gameStatus = endGameState.getGameStatus();

        //Assert
        Assert.assertEquals("The game should now be over due to a positive gradient diagonal win", GameStatus.RED_WIN, gameStatus);

    }

    /**
     * Checks to ensure the game logic behaves correctly when the gird and win sizes are scaled up.
     * Vertical win
     * Game state under test:
     * 14|  |  |  |  |  |  |  |  |  |  |  |
     * 13|Y?|  |  |  |  |  |  |  |  |  |  |
     * 12|Y |  |  |  |  |  |  |  |  |  |  |
     * 11|Y |R |  |  |  |  |  |  |  |  |  |
     * 10|Y |R |  |  |  |  |  |  |  |  |R |
     * 9 |Y |R |  |  |  |  |  |  |  |R |R |
     * 8 |Y |R |  |  |  |  |  |  |R |Y |Y |
     * 7 |Y |R |  |  |  |  |  |R |Y |R |R |
     * 6 |Y |R |  |  |  |  |R |R |R |Y |Y |
     * 5 |Y |R |  |  |  |R |R |Y |Y |R |R |
     * 4 |Y |R |  |  |R |Y |Y |R |R |Y |Y |
     * 3 |Y |R |  |R |Y |R |R |Y |Y |R |R |
     * 2 |Y |R |R |R |R |Y |Y |R |R |Y |Y |
     * 1 |Y |R |R |Y |Y |R |R |Y |Y |R |R |
     * 0 |R |Y |Y |Y |Y |Y |Y |Y |Y |Y |Y |
     * ...0  1  2  3  4  5  6  7  8  9  10
     */
    @Test
    public void check11x15Connect12() {

        //Setup
        //1) Define the large grid and win condition
        final int gameWidth = 11;
        final int gameHeight = 15;
        final int consecutivePiecesRequiredForWin = 12;

        //2) Create a game state one move away from a win
        List<GamePiece> preExistingPieces = createInitialPieceListFor11X15Connect12();
        gamePieceSet.addAll(preExistingPieces);

        GameState startingGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        //Test
        //1)Check GameStatus is undetermined.
        GameStatus gameStatus = startingGameState.getGameStatus();
        Assert.assertEquals("There shouldn't be a vertical win yet", GameStatus.UNDETERMINED, gameStatus);

        //2)Play the move which will end the game.
        GamePiece finalPiece = new GamePieceBuilder()
                .withxPosition(0)
                .withyPosition(13)
                .withPieceColour(PieceColour.YELLOW)
                .build();
        gamePieceSet.add(finalPiece);

        GameState endGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        gameStatus = endGameState.getGameStatus();

        //Assert
        Assert.assertEquals("The game should now be over due to a vertical win", GameStatus.YELLOW_WIN, gameStatus);

    }

    /**
     * Checks to ensure the game logic behaves correctly when the gird and win sizes are scaled up.
     * Horizontal win
     * Game state under test:
     * 10|  |  |  |  |  |  |  |  |  |  |R |
     * 9 |  |  |  |  |  |  |  |  |  |R |R |
     * 8 |  |  |  |  |  |  |  |  |R |Y |Y |
     * 7 |  |  |  |  |  |  |  |R |Y |R |R |
     * 6 |  |  |  |  |  |  |R |R |R |Y |Y |
     * 5 |  |  |  |  |  |R |R |Y |Y |R |R |
     * 4 |  |  |  |  |R |Y |Y |R |R |Y |Y |
     * 3 |  |  |  |R |Y |R |R |Y |Y |R |R |
     * 2 |  |  |R |R |R |Y |Y |R |R |Y |Y |
     * 1 |Y |  |R |Y |Y |R |R |Y |Y |R |R |
     * 0 |R |Y?|Y |Y |Y |Y |Y |Y |Y |Y |Y |
     * ...0  1  2  3  4  5  6  7  8  9  10
     */
    @Test
    public void check11x10connect10() {

        //Setup
        //1) Define the large grid and win condition
        final int gameWidth = 11;
        final int gameHeight = 11;
        final int consecutivePiecesRequiredForWin = 10;

        //2) Create a game state one move away from a win
        List<GamePiece> preExistingPieces = createInitialPieceListFor11x10Connect10();
        gamePieceSet.addAll(preExistingPieces);

        GameState startingGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.RED)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        //Test
        //1)Check GameStatus is undetermined.
        GameStatus gameStatus = startingGameState.getGameStatus();
        Assert.assertEquals("There shouldn't be a horizontal win yet", GameStatus.UNDETERMINED, gameStatus);

        //2)Play the move which will end the game.
        GamePiece finalPiece = new GamePieceBuilder()
                .withxPosition(1)
                .withyPosition(0)
                .withPieceColour(PieceColour.YELLOW)
                .build();
        gamePieceSet.add(finalPiece);

        GameState endGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        gameStatus = endGameState.getGameStatus();

        //Assert
        Assert.assertEquals("The game should now be over due to a vertical win", GameStatus.YELLOW_WIN, gameStatus);

    }

    /**
     * Checks to ensure the game logic behaves correctly when the gird and win sizes are scaled up.
     * Negative gradient win
     * Game state under test:
     * 8 |R?|  |  |  |  |  |  |  |  |
     * 7 |Y |R |  |  |  |  |  |  |  |
     * 6 |R |Y |R |  |  |  |  |  |  |
     * 5 |Y |Y |Y |R |  |  |  |  |  |
     * 4 |R |R |Y |Y |R |  |  |  |  |
     * 3 |Y |Y |R |R |Y |R |  |  |  |
     * 2 |R |R |Y |Y |R |Y |R |  |  |
     * 1 |Y |Y |R |R |Y |Y |R |R |  |
     * 0 |R |R |Y |Y |R |Y |Y |Y |R |
     * ...0  1  2  3  4  5  6  7  8
     */
    @Test
    public void check9x9connect9() {

        //Setup
        //1) Define the large grid and win condition
        final int gameWidth = 9;
        final int gameHeight = 9;
        final int consecutivePiecesRequiredForWin = 9;

        //2) Create a game state one move away from a win
        List<GamePiece> preExistingPieces = createInitialPieceListFor9x9Connect9();
        gamePieceSet.addAll(preExistingPieces);

        GameState startingGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        //Test
        //1)Check GameStatus is undetermined.
        GameStatus gameStatus = startingGameState.getGameStatus();
        Assert.assertEquals("There shouldn't be a negative gradient diagonal win yet", GameStatus.UNDETERMINED, gameStatus);

        //2)Play the move which will end the game.
        GamePiece finalPiece = new GamePieceBuilder()
                .withxPosition(0)
                .withyPosition(8)
                .withPieceColour(PieceColour.RED)
                .build();
        gamePieceSet.add(finalPiece);

        GameState endGameState = new GameStateBuilder()
                .withGameWidth(gameWidth)
                .withGameHeight(gameHeight)
                .withColourPlayed(PieceColour.YELLOW)
                .withConsecutivePiecesForWin(consecutivePiecesRequiredForWin)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();

        gameStatus = endGameState.getGameStatus();

        //Assert
        Assert.assertEquals("The game should now be over due to a negative gradient diagonal win", GameStatus.RED_WIN, gameStatus);

    }


    //GamePieceSet setup methods.

    /**
     * Creates the initial piece list required for check15x15Connect15 test
     *
     * @return desired GamePiece List
     */
    private List<GamePiece> createInitialPieceListFor15x15Connect15() {
        return Arrays.asList(

                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(7).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(8).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(10).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(7).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(8).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(9).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(10).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(11).withyPosition(11).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(7).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(8).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(9).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(10).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(11).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(12).withyPosition(12).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(10).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(11).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(12).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(13).withyPosition(13).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(10).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(11).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(12).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(14).withyPosition(13).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(1).withPieceColour(PieceColour.YELLOW).build()

        );
    }

    /**
     * Creates the initial piece list required for check11x15Connect12 test
     *
     * @return desired GamePiece List
     */
    private List<GamePiece> createInitialPieceListFor11X15Connect12() {
        return Arrays.asList(

                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(7).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(8).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(10).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(7).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(9).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(8).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(10).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(11).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(10).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(12).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(11).withPieceColour(PieceColour.RED).build()


        );
    }

    /**
     * Creates the initial piece list required for check11x10Connect10 test
     *
     * @return desired GamePiece List
     */
    private List<GamePiece> createInitialPieceListFor11x10Connect10() {
        return Arrays.asList(

                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(7).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(8).withyPosition(8).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(9).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(8).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(9).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(10).withyPosition(10).withPieceColour(PieceColour.RED).build()

        );
    }

    /**
     * Creates the initial piece list required for check9x9Connect9 test
     *
     * @return desired GamePiece List
     */
    private List<GamePiece> createInitialPieceListFor9x9Connect9() {
        return Arrays.asList(

                new GamePieceBuilder().withxPosition(8).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(7).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(6).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(5).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(4).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(3).withyPosition(5).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(0).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(1).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(2).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(3).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(4).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(2).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(0).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(6).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(1).withyPosition(7).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(1).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(2).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(3).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(4).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(5).withPieceColour(PieceColour.YELLOW).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(6).withPieceColour(PieceColour.RED).build(),
                new GamePieceBuilder().withxPosition(0).withyPosition(7).withPieceColour(PieceColour.YELLOW).build()

        );
    }

}
