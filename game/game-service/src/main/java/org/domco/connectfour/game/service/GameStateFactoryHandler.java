package org.domco.connectfour.game.service;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.domco.connectfour.game.domain.GamePiece;
import org.domco.connectfour.game.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.game.domain.GameState;
import org.domco.connectfour.game.domain.GameState.GameStateBuilder;
import org.domco.connectfour.game.domain.PieceColour;
import org.springframework.stereotype.Service;

/**
 * Game State Factory Handler
 */
@Service
public class GameStateFactoryHandler implements GameStateFactory {

    /**
     * Width of a standard game.
     * For standard rules set equal to 7
     */
    private static final int STANDARD_GAME_WIDTH = 7;

    /**
     * Height of a standard game.
     * For standard rules set equal to 6
     */
    private static final int STANDARD_GAME_HEIGHT = 6;

    /**
     * Number of consecutive pieces that are the same colour required for the game to be won.
     * For standard rules set equal to 4
     */
    private static final int CONSECUTIVE_PIECES_FOR_WIN = 4;

    /**
     * The handicap variable dictates how many turns player two gets for each of player ones.
     * When HANDICAP = 0. Player two gets 1 turn for each of player ones turn
     * When HANDICAP = 1. Player two gets 2 turns for each of player ones turn
     * When HANDICAP = 2. Player two gets 3 turns for each of player ones turn
     * etc...
     */
    private static final int HANDICAP = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    public GameState createEmptyStandardGameState() {

        final Set<GamePiece> gamePieceSet = new HashSet<>();
        return new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withPlayerTwoHandicap(HANDICAP)
                .withGamePieceSet(gamePieceSet)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GameState createRandomStandardGameState() {

        final Random random = new Random();

        final int x = random.nextInt(STANDARD_GAME_WIDTH);
        final int y = random.nextInt(STANDARD_GAME_HEIGHT);

        PieceColour pieceColour;
        if (random.nextBoolean()) {
            pieceColour = PieceColour.RED;
        } else {
            pieceColour = PieceColour.YELLOW;
        }

        final GamePiece gamePiece = new GamePieceBuilder()
                .withxPosition(x)
                .withyPosition(y)
                .withPieceColour(pieceColour)
                .build();

        final Set<GamePiece> gamePieceSet = new HashSet<>();
        gamePieceSet.add(gamePiece);

        return new GameStateBuilder()
                .withGameWidth(STANDARD_GAME_WIDTH)
                .withGameHeight(STANDARD_GAME_HEIGHT)
                .withConsecutivePiecesForWin(CONSECUTIVE_PIECES_FOR_WIN)
                .withGamePieceSet(gamePieceSet)
                .build();
    }

}
